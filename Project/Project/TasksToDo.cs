﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project
{

	class TasksToDo
	{
		public async Task<Info> GetInfo()
		{
			var users = await new Repository().GetUsers();
			var projects = await new Repository().GetProjects();
			var teams = await new Repository().GetTeams();
			var tasks = await new Repository().GetTasks();

			var info = new Info(users, teams, tasks, projects);
			return info;
		}

		public async Task<IEnumerable<IGrouping<Project, Task>>> GetUserTaskById(int id) //#1
		{
			try
			{
				var tasks = await GetInfo();
				var taskList = tasks.GetTasks();

				var result = from task in taskList
							 where task.PerformerId == id
							 group task by task.Project;

				return result;
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
				return null;
			}
			
		}

		public async Task<IEnumerable<Task>> GetTasksShortName(int id) //#2
		{
			var tasks = await GetInfo();
			var taskList = tasks.GetTasks();

			var result = from task in taskList
						 where task.PerformerId == id
						 where task.Name.Length < 45
						 select task;

			return result;
		}

		public async Task<IEnumerable<TaskOut>> GetTasksFinished2021(int id) //#3
		{
			var tasks = await GetInfo();
			var taskList = tasks.GetTasks();

			var result = from task in taskList
						 where task.FinishedAt.HasValue == true
						 where task.PerformerId == id
						 where task.FinishedAt.Value.Year == 2021
						 select new TaskOut
						 {
							 Id = task.Id,
							 Name = task.Name
						 };

			return result;
		}

		public async Task<IEnumerable<IGrouping<Team, User>>> GetUsersOlder10Years() //#4
		{
			var users = await GetInfo();
			var usersList = users.GetUsers();

			var result = from user in usersList
						 where user.BirthDay.Year < (DateTime.Now.Year - 10)
						 orderby user.RegisteredAt descending
						 group user by user.Team;

			return result.ToList();
		}

		public async Task<IEnumerable<IGrouping<User, Task>>> GetSortedUsersSortedTasks() //#5
		{
			var tasks = await GetInfo();
			var taskList = tasks.GetTasks();

			var result = from task in taskList
						 orderby task.Performer.FirstName ascending
						 group task by task.Performer;

			return result;
		}

		public async Task<IEnumerable<Task6>> Task6(int id)
		{
			var tasks = await GetInfo();
			var info = tasks.GetTasks();

			var result = info.Select(p => new Task6
			{
				User = info
					.Where(i => i.Performer.Id == id)
					.Select(i => i.Performer)
					.FirstOrDefault(),
				LastProject = info
					.Where(i => i.Performer.Id == id)
					.OrderBy(i => i.Project.CreatedAt)
					.Select(i => i.Project)
					.LastOrDefault(),
				TaskCount = info
					.Where(i => i.Performer.Id == id)
					.OrderBy(i => i.Project.CreatedAt)
					.Select(i => i).Count(),
				CanceledOrInProgressTask = info
					.Where(i => i.State == TaskStates.Canceled || i.State == TaskStates.InProgress)
					.Count(),
				LongestTask = info
					.OrderBy(i => (i.FinishedAt - i.CreatedAt)
					.GetValueOrDefault())
					.LastOrDefault()
			});

			return result;

		}
	}
}

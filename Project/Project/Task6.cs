﻿using System;

namespace Project
{
	public class Task6
	{
		public User User { get; set; }
		public Project LastProject { get; set; }
		public int TaskCount { get; set; }
		public int CanceledOrInProgressTask { get; set; }
		public Task LongestTask { get; set; }

		public async System.Threading.Tasks.Task DisplayInfo()
		{
			Console.WriteLine(User.FirstName + " " + User.LastName);
			Console.WriteLine(LastProject.Id + " " + LastProject.Name);
			Console.WriteLine(TaskCount);
			Console.WriteLine(CanceledOrInProgressTask);
			Console.WriteLine(LongestTask.Name + " " + LongestTask.CreatedAt + " " + LongestTask.FinishedAt);
		}

	


	}
}

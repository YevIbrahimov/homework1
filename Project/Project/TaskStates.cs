﻿namespace Project
{
    public enum TaskStates
    {
        ToDo,
        InProgress,
        Done,
        Canceled
    }
}

﻿using System.Collections.Generic;
using System.Linq;

namespace Project
{
	class Info
	{
		private List<User> users = new List<User>();
		private List<Team> teams = new List<Team>();
		private List<Task> tasks = new List<Task>();
		private List<Project> projects = new List<Project>();

		public Info(List<User> users, List<Team> teams, List<Task> tasks, List<Project> projects)
		{
			this.users = users;
			this.teams = teams;
			this.tasks = tasks;
			this.projects = projects;
		}

		public List<User> GetUsers()
		{
			users = users.Join(teams,
				u => u.TeamId,
				t => t.Id,
				(u, t) =>
				{
					u.Team = t;
					return u;
				}).ToList();

			return users;
		}

		public List<Task> GetTasks()
		{
			tasks = tasks.Join(GetUsers(),
				t => t.PerformerId,
				u => u.Id,
				(t, u) => 
				{
					t.Performer = u;
					return t;
				}).Join(projects,
				t => t.ProjectId,
				p => p.Id,
				(t, p) =>
				{
					t.Project = p;
					return t;
				}).ToList();

			return tasks;
		}
	}
}

﻿using System;
using System.Text.RegularExpressions;

namespace Project
{
	class Program
	{
		static async System.Threading.Tasks.Task Main(string[] args)
		{
			await UserInterface();
		}

		static private async System.Threading.Tasks.Task UserInterface()
		{
			var task = new TasksToDo();

			while (true)
			{
				Console.WriteLine("enter option[1-7] or exit");
				var userInput = Console.ReadLine();

				if (userInput.ToUpper() == "EXIT")
				{
					break;
				}

				if (Int32.Parse(userInput) < 1 || Int32.Parse(userInput) > 7)
				{
					Console.WriteLine("no such option");
				}
				else
				{
					switch (Int32.Parse(userInput))
					{
						case 1:
							if(IdEnter() != null)
							{
								var task1 = await task.GetUserTaskById((int)IdEnter());
							}
							break;
						case 2:
							if (IdEnter() != null)
							{
								var task2 = await task.GetTasksShortName((int)IdEnter());
								foreach (var item in task2)
								{
									Console.WriteLine($"{item.Id} {item.Name}");
								}
							}
							break;
						case 3:
							if (IdEnter() != null)
							{
								var task3 = await task.GetTasksFinished2021((int)IdEnter());
								foreach (var item in task3)
								{
									Console.WriteLine($"{item.Id} {item.Name}");
								}
							}
							break;
						case 4:
							var task4 = await task.GetUsersOlder10Years();
							foreach (var item in task4)
							{
								Console.WriteLine($"{item.Key.Id} {item.Key.Name}");
								foreach (var user in item.Key.Users)
								{
									Console.WriteLine($"{user.FirstName} {user.LastName} {user.BirthDay}");
								}
							}
							break;
						case 5:
							var task5 = await task.GetSortedUsersSortedTasks();
							foreach (var item in task5)
							{
								Console.WriteLine($"{item.Key.FirstName}");
								foreach (var taskItem in item.Key.Tasks)
								{
									Console.WriteLine($"{taskItem.Name}");
								}
							}
							break;
						case 6:
							if (IdEnter() != null)
							{
								var task6 = await task.Task6((int)IdEnter());
							}
							break;
						default:
							break;
					}
				}

			}
		}

		private static int? IdEnter()
		{
			Console.WriteLine("enter id: ");
			var idInput = Console.ReadLine();
			if (Int32.TryParse(idInput, out int result))
			{
				if (result < 0 && result > 122)
				{
					Console.WriteLine("wrong id");
				}
				else
				{
					return result;
				}
				
			}
			else
			{
				Console.WriteLine("wrong id");
			}
			return null;
		}
	}
}

﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Project
{
	class Repository
	{
		private readonly HttpClient client;
		public Repository()
		{
			client = new HttpClient();
		}
		
		public async Task<List<Project>> GetProjects()
		{
			var response = await client.GetAsync("https://bsa21.azurewebsites.net/api/Projects");
			var content = await response.Content.ReadAsStringAsync();
			var projects = JsonConvert.DeserializeObject<List<Project>>(content);

			return projects;
		}

		public async Task<List<Task>> GetTasks()
		{
			var response = await client.GetAsync("https://bsa21.azurewebsites.net/api/Tasks");
			var content = await response.Content.ReadAsStringAsync();
			var tasks = JsonConvert.DeserializeObject<List<Task>>(content);

			return tasks;
		}

		public async Task<List<User>> GetUsers()
		{
			var response = await client.GetAsync("https://bsa21.azurewebsites.net/api/Users");
			var content = await response.Content.ReadAsStringAsync();
			var users = JsonConvert.DeserializeObject<List<User>>(content);

			return users;
		}

		public async Task<List<Team>> GetTeams()
		{
			var response = await client.GetAsync("https://bsa21.azurewebsites.net/api/Teams");
			var content = await response.Content.ReadAsStringAsync();
			var teams = JsonConvert.DeserializeObject<List<Team>>(content);

			return teams;
		}
	}
}
